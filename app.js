const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerJSDocs = require('swagger-jsdoc');
const router = require('../feature-toggle/modules/users/user.controller');
const swagger = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swagger));


// app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/users/', (req, res) => {
    res.send(router.get('/'));
});

// Routes

/**
 * @swagger
 * /users:
 *  get:
 *      description: Use to request all users
 *      produces:
 *        - application/json
 *      parameters:
 *       - name: username
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *      responses:
 *          '200':
 *              description: A successful message
 *  put:
 *      description: Use to request all users
 *      responses:
 *          '200':
 *              description: A successful message
 */


app.listen(5000, () => {
    console.log('Listening on port 5000');
});
