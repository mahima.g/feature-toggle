const expressJwt = require('express-jwt');
const config = require('../config/config');
const userService = require('../modules/users/user.service');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/auth/authenticate',
            '/auth/register',
            '/aboutUs',
            '/privacy-policy',
            '/terms-conditions',
            '/api-docs'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};