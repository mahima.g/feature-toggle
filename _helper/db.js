const mongoose = require('mongoose');
const auth = process.env.USERNAME ? `${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@` : ''
const connectionString = `mongodb://${auth}${process.env.DB_HOST}/${process.env.DB_NAME}`;
mongoose.connect(connectionString, { useCreateIndex: true, useNewUrlParser: true });


module.exports = {
    User: require('../modules/users/user.model')
};
