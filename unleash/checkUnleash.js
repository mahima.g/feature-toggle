const { initialize,
    isEnabled,
    getVariant,
    getFeatureToggleDefinition,
    getFeatureToggleDefinitions
} = require('unleash-client');

const instance = initialize({
    url: 'http://localhost:4242/api',
    appName: 'my-app'
});

// optional events
instance.on('error', console.error);
instance.on('warn', console.warn);
instance.on('ready', console.log);

instance.on('registered', clientData => console.log('registered', clientData));
instance.on('sent', payload => console.log('metrics bucket/payload sent', payload));
instance.on('count', (name, enabled) => console.log(`isEnabled(${name}) returned ${enabled}`));


module.exports = function (req, res, next) {

    console.log(getFeatureToggleDefinitions());
    console.log('==>', isEnabled('userProfile'));

    const context = {
        userId: '5e68be4a3189b4279c4542a4'
        // sessionId: '000' + global.fakeId,
        // remoteAddress: '127.0.0.1'
    };

    const enabled1 = isEnabled('userProfile', context);
    const enabled = isEnabled('privacy-policy', context);
    if (enabled) {
        const variant = getVariant('userProfile', context);
        console.log('VARIANT ====>>> ', variant.name, ' ', variant.payload);
        next();
    }
    else {
        res.status(503).send('This feature is disabled for you!');
    }


};
