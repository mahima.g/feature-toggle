/* Initialize the unleash client */
const { initialize } = require('unleash-client');
const instance = initialize({
    //   url: config.get('unleash').url,
    //   appName: config.get('unleash').app_name
    url: 'http://192.168.3.69:4242/api/admin',
    appName: 'feature-toggle-demo'

});

/* Optional events */
instance.on('error', console.error);
instance.on('warn', console.warn);
instance.on('ready', console.log);
// console.log('hostname', process.env.HOSTNAME)

/* Metric Hooks */
instance.on('registered', clientData => { console.log('registered', clientData) });
// instance.on('sent', payload => console.log('metrics bucket/payload sent', payload));
// instance.on('count', (name, enabled) => console.log(`isEnabled(${name}) returned ${enabled}`));

module.exports = instance;
