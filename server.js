const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const errorHandler = require('../feature-toggle/_helper/error-handler');
app.use(require('cors')());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
require('dotenv').config({ path: process.env.NODE_ENV + '.env' });
const unleashMiddleware = require('./unleash/checkUnleash');
// api routes
app.use('/auth', require('./modules/auth/auth.routes.js'));
app.use('/users', require('./modules/users/user.routes.js'));
app.use('/aboutUs', require('../feature-toggle/utilities/about-us'));
app.use('/terms-conditions', require('../feature-toggle/utilities/terms-Conditions'));
app.use('/privacy-policy', unleashMiddleware, require('../feature-toggle/utilities/privacy-policy'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.PORT || 4000;
const test = app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log('Server listening on port ' + port);
});

module.exports = test;
