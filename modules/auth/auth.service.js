const config = require('../../config/config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../../_helper/db');
const User = db.User;

module.exports = {
    authenticate,
    create
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compare(password, user.password)) {
        const { password, ...userWithoutpassword } = user.toObject();
        const token = jwt.sign({ sub: user.id, exp: new Date().getTime() + 60 * 60 * 24 }, config.secret);
        return {
            status: 1,
            userWithoutpassword,
            token
        };
    }
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username, })) {
        const err = {
            status: 0,
            message: 'Username "' + userParam.username + '" is already taken'
        }
        throw err;

    }



    const user = new User(userParam);

    // hash password
    if (userParam.password) {

        user.password = await bcrypt.hash(userParam.password, 10);
    }

    // save user
    await user.save();
}