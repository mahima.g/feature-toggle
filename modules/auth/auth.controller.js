const express = require('express');
const userService = require('./auth.service');


function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ status: 0, message: 'Invalid credentials' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.status(200).send({
            status: 1,
            message: 'User inserted successfully!'
        }))
        .catch(err => next({ status: 0, message: err }));
}

module.exports = {
    authenticate,
    register
};
