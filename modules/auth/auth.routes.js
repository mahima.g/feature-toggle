const express = require('express');
const router = express.Router();
const controller = require('../auth/auth.controller');
// routes

router.post('/authenticate', controller.authenticate);
router.post('/register', controller.register);

module.exports = router;
