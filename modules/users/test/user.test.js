const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const bcrypt = require('bcryptjs');
const request = require('supertest');
const TestCaseSignUp = require('../testcase/testcaseSignUp');
const TestCaseSignIn = require('../testcase/testcaseSignIn');
const TestCaseDBOps = require('../testcase/testcaseDBops');
const TRUE_DATA_STATUS = 1;
const FALSE_DATA_STATUS = 0;
const app = require('../../../server');
let requestPayload;
const User = require('../user.model.js');
const UserSeed = require('./seed/user.seed.js');
request(app);

describe('Signup Account', () => {
    before(async () => {
        await User.insertMany(UserSeed);
    });

    after(async () => {
        await User.remove();
    });

    TestCaseSignUp.registerAccount.forEach((data) => {
        it(data.it, async () => {
            const res = await request(process.env.BASE_URL)
                .post('/auth/register')
                .send(data.options);
            assert.equal(res.body.status, data.status);
        });
    });

    TestCaseSignUp.verifyAccount.forEach((data) => {
        it(data.it, async () => {
            const res = await request(process.env.BASE_URL)
                .post('/auth/register')
                .send(data.options);
            expect(res.body.status).to.be.status;
            assert.equal(res.body.status, FALSE_DATA_STATUS);
        });
    });
});

describe('SignIn Account', () => {
    before(async () => {
        const users = [];
        let user;
        for (let index = 0; index < UserSeed.length; index++) {
            user = UserSeed[index];
            user.password = await bcrypt.hash(user.password, 10);
            users.push(user);
        }
        await User.insertMany(users);
    });

    after(async () => {
        await User.remove();
    });

    TestCaseSignIn.loginAccount.forEach((data) => {
        it(data.it, async () => {
            // const body = data.options;
            // body.password = await bcrypt.hash(body.password, 10);
            // data.options.password = body.password;
            const res = await request(process.env.BASE_URL)
                .post('/auth/authenticate')
                .send(data.options);
            //console.log("RESPONSE ::::", res.body.token);
            requestPayload = res.body;
            // console.log('PAYLOAD :::', requestPayload.token);
            assert.equal(res.body.status, data.status);
        });
    });
});

describe('DB Operations', () => {
    // before(async () => {
    //     await User.insertMany(UserSeed);
    // });

    // after(async () => {
    //     await User.remove();
    // });

    TestCaseDBOps.DBOperation.forEach((data) => {
        it(data.it, async () => {
            const res = await request(process.env.BASE_URL)
                .get('/users/')
                .set({ Authorization: 'Bearer ' + requestPayload.token });
            assert.equal(res.body.status, data.status);
        });
    });
});
