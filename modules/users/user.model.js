const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    // _id: { Object },
    username: { type: String, unique: true },
    password: { type: String },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
