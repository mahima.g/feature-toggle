module.exports = {
    registerAccount: [
        {
            it: 'As a user I should validate if username is not pass',
            options: {
                'username': '',
                'password': 'm'
            },
            stauts: 0
        },
        {
            it: 'As a user I should validate if password is not pass',
            options: {
                'username': 'maasdfg123456',
                'password': ''
            },
            stauts: 0
        },
        {
            it: 'As a user I should validate if password is greater than 5 character',
            options: {
                'username': 'm1234',
                'password': 'm'
            },
            stauts: 0
        },
        {
            it: 'As a user I should validate if username is not numeric',
            options: {
                'username': '12345676',
                'password': 'm'
            },
            stauts: 0
        },
        {
            it: 'As a user I should validate if username is not already taken',
            options: {
                'username': '12345676',
                'password': 'm'
            },
            stauts: 0
        }

    ],
    verifyAccount: [
        {
            it: 'As a user, I should check blank user.',
            options: {
                'username': ''
            },
            status: 0
        },
        {
            it: 'As a user, I should check invalid user.',
            options: {
                'username': 'test'
            },
            status: 0
        },
        {
            it: 'As a user, I should check non existing user.',
            options: {
                'username': 'testest'
            },
            status: 0
        }

    ]
}