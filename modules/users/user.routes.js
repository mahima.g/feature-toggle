const express = require('express');
const router = express.Router();
const Usercontroller = require('../users/user.controller');
const app = express();
const bodyParser = require('body-parser');
const jwt = require('../../_helper/jwt.js');

app.use(require('cors')());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// routes

router.use(jwt());
router.get('/', Usercontroller.getAll);
router.get('/current', Usercontroller.getCurrent);
router.get('/:id', Usercontroller.getById);
router.put('/:id', Usercontroller.update);
router.delete('/:id', Usercontroller._delete);

module.exports = router;
