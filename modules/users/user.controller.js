const express = require('express');
const userService = require('./user.service');




function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    // eslint-disable-next-line no-console
    // const token = req.header('Authorization').replace(/^Bearer\s/, '');
    // const decoded = jwt.verify(token, config.secret);
    // console.log(decoded.sub);
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.status(404).send({ status: 0, message: 'User not found' }))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.status(200).send({ status: 1, message: 'User deleted' }))
        .catch(err => next(err));
}

module.exports = {
    getAll,
    getById,
    getCurrent,
    update,
    _delete
}