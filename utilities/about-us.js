const express = require('express');
const router = express.Router();

router.get('/', getdetails);

module.exports = router;

function getdetails(req, res) {
    const data = {
        title: 'About us',
        description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
            + ' Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
            + ' when an unknown printer took a galley of type and scrambled it to make a type specimen book.'
    };
    res.status(200).send(data);
}
